/* Antti Vilkman 0521281
 * CT30A3370 Käyttöjärjestelmät ja systeemiohjelmointi
 * 12/2018
 * Harjoitustyö Kernel Hacking
 * getreadcount.c
 *
 * Ohjelma kutsuu getreadcount-systeemikutsua ja tulostaa näytölle read-kutsujen määrän käyttöjärjestelmän suorituksen aikana
 */


#include "types.h"
#include "stat.h"
#include "user.h"


int main(int argc, char *argv[]) {
	if (argc > 1) {
		printf(1, "%d\n", getreadcount(1));
	}
	else {
		printf(1, "%d\n", getreadcount(0));
	}
	exit();
}